import Server from '@root/Server'
import { getEnv } from '@root/utils'
// import cors from 'cors'
// const app = express().use('*', cors())

// Declare some constants
const port = parseInt(getEnv('PORT'), 10) || 3000
const mode = process.env.NODE_ENV


const s: Server = new Server(port, mode)
s.start()

/*
// import express from 'express'
// import {
//   ApolloServer
// } from 'apollo-server-express'
// import bodyParser from 'body-parser'
// import cors from 'cors'
// import { getEnv } from '@root/utils'
// import { SubscriptionServer } from 'subscriptions-transport-ws'
// import { execute, subscribe } from 'graphql'
// import { createServer } from 'http'
// import { PubSub } from 'graphql-subscriptions'


// // Enable CORS
// const app = express().use('*', cors())

// // Parse body
// app.use(bodyParser.json())

const typeDefs = augmentTypeDefs(schema, {
  resolvers
})
// // Consolidate form and request data into one place
// app.use((req, res, next) => {
//   req.body = { ...req.body, ...req.params, ...req.query }
//   next()
// })

// // Heartbeat endpoint
// app.get('/heartbeat', (req, res) => {
//   res.send('Thump Thump')
// })
// 1
*/
