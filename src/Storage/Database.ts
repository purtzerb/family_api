import { getEnv } from '@root/utils'
import neo4j from 'neo4j-driver'
import { transformDatabaseReturn } from '@root/utils/transforms'

export default class Database {
  url: string

  userName: string

  password: string

  port: number

  private driver: object

  constructor(url: string, port: number, userName: string, password: string) {
    this.url = url
    this.port = port
    this.userName = userName
    this.password = password
  }

  /**
 * @method getDriver Gets the correct driver for the database
 * @returns {Object} Driver
 */
  getDriver() {
    if (!this.driver) {
      this.createDriver()
    }
    return this.driver
  }

  runQuery(query: string, dbName: string) {
    if (!this.driver) {
      this.createDriver()
    }
    const session = this.driver.session({ database: dbName })
    const res = session.run(query)
    session.close()
    return transformDatabaseReturn(res)
  }

  private createDriver() {
    this.driver = neo4j.driver(
      `bolt://${getEnv('DATABASE_URI')}`,
      neo4j.auth.basic(getEnv('DATABASE_USERNAME'), getEnv('DATABASE_PASSWORD'))
    )
  }
}
