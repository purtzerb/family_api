import { getEnv } from '@root/utils'
import { PubSub } from 'graphql-subscriptions'
import Database from './Database'

let dbHolder
const getDB = () => {
  dbHolder = new Database(getEnv('DATABASE_URI'), getEnv('PORT'), getEnv('DATABASE_USERNAME'), getEnv('DATABASE_PASSWORD'))
  return dbHolder
}

// eslint-disable-next-line import/prefer-default-export
export const DB = dbHolder || getDB()

let ps
const makePS = () => {
  ps = new PubSub()
  return ps
}
export const pubSub = ps || makePS()
