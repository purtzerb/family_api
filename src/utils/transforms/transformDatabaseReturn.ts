export default (res) => {
  if (res.records.length) {
    if (res.records.length > 1) {
      return res.records.map((rec) => rec.get(0).properties)
    }
    return res.records[0].get(0).properties
  }
  return { name: 'No users' }
}
