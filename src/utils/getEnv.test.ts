import getEnv from './getEnv'

describe('getEnv for multiple environments', () => {
  beforeAll(() => {
    process.env.DEV_TEST = 'DEVELOPMENT TEST'
    process.env.STAGING_TEST = 'STAGING TEST'
    process.env.PROD_TEST = 'PRODUCTION TEST'
  })

  it('should work for dev', () => {
    process.env.NODE_ENV = 'development'
    expect(getEnv('TEST')).toStrictEqual('DEVELOPMENT TEST')
  })

  it('should work for staging', () => {
    process.env.NODE_ENV = 'staging'
    expect(getEnv('TEST')).toStrictEqual('STAGING TEST')
  })

  it('should work for production', () => {
    process.env.NODE_ENV = 'production'
    expect(getEnv('TEST')).toStrictEqual('PRODUCTION TEST')
  })
})
