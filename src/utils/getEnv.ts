/**
 * This function is used to get environmental variables for each build phase (development, staging, production).
 * @param {STRING} NAME is used to fetch the specific environmental variable
 * @returns {String} The value of the named variable. Can be undefined.
 */
const getEnv = (NAME: string) => {
  switch (process.env.NODE_ENV) {
    case 'development':
      return process.env[`DEV_${NAME}`]
    case 'staging':
      return process.env[`STAGING_${NAME}`]
    default:
      return process.env[`PROD_${NAME}`]
  }
}

export default getEnv
