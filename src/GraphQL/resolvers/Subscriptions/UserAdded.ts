export default {
  // Additional event labels can be passed to asyncIterator creation
  subscribe: (object, params, ctx) => ctx.pubsub.asyncIterator(['USER_ADDED']),
  resolve: payload => payload
}
