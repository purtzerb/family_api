import { neo4jgraphql } from 'neo4j-graphql-js'

export default (object, params, ctx, resolveInfo) => neo4jgraphql(object, params, ctx, resolveInfo)
