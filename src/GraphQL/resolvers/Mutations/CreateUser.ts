import { neo4jgraphql } from 'neo4j-graphql-js'

export default (object, params, ctx, resolveInfo) => {
  ctx.pubsub.publish('USER_ADDED', {
    name: params.name
  })
  return neo4jgraphql(object, params, ctx, resolveInfo)
}
