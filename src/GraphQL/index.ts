// / <reference path="./graphql.d.ts" />
import { augmentTypeDefs, makeAugmentedSchema } from 'neo4j-graphql-js'
import schemaFile from './schema.graphql'
import * as Queries from './resolvers/Queries'
import * as Mutations from './resolvers/Mutations'
import * as Subscriptions from './resolvers/Subscriptions'

// Create resolver object
let r
const makeResolvers = () => {
  r = {
    Query: Queries,
    Mutation: Mutations,
    Subscription: Subscriptions
  }
  return r
}
export const resolvers = r || makeResolvers()

/*
Augmented scheme can be undone by using typedefs and
resolvers within GraphQLServer declaration within the Server.ts file
*/
export const schema = makeAugmentedSchema({
  typeDefs: augmentTypeDefs(schemaFile),
  resolvers
})
