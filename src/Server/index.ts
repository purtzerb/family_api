import { GraphQLServer } from 'graphql-yoga'
import { resolvers, schema } from '@root/GraphQL'
import { pubSub, DB } from '@root/Storage'
import { authorize } from '@root/utils/middleware'

export default class Server {
  port: number

  mode: string

  constructor(port: number, mode: string) {
    this.port = port
    this.mode = mode
  }

  start() {
    const s = new GraphQLServer({
      schema,
      resolvers,
      context: {
        driver: DB.getDriver(),
        pubsub: pubSub
      },
      formatparams: (...rest) => {
        console.log(rest)
      }
    })
    s.express.use('*', authorize)
    s.start({
      tracing: this.mode !== 'production',
      port: this.port || 3000
    }, () => {
      // eslint-disable-next-line no-console
      console.log(`Server is running on http://localhost:${this.port} in ${this.mode} mode`)
    })
  }
}
